package com.qa.automation.fossil.question.question1.Objects;

public class Car {
    public int wheels;
    public int doors;
    public int seats;
    public int maxSpeed;

    public Car() {
        this.wheels = 4;
        this.doors = 4;
        this.seats = 5;
    }


    public void run() {
        for (int i=0; i < 10; i++) {
            System.out.println(this.maxSpeed);
        }
    }
}

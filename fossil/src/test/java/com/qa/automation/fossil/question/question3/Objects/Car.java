package com.qa.automation.fossil.question.question3.Objects;

public class Car implements Runnable{
    public int wheels;
    public int doors;
    public int seats;
    public int maxSpeed;

    public Car() {
        this.wheels = 4;
        this.doors = 4;
        this.seats = 5;
    }

    @Override
    public void run() {
        for (int i=0; i < 10; i++) {
            System.out.println(this.maxSpeed);
            try {
                Thread.sleep(50);
            } catch (InterruptedException e ) {
                e.printStackTrace();
            }
        }
    }
}

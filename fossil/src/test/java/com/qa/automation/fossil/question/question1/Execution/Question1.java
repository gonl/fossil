package com.qa.automation.fossil.question.question1.Execution;

import com.qa.automation.fossil.question.question1.Objects.CarBMW;
import com.qa.automation.fossil.question.question1.Objects.CarToyota;

public class Question1 {

    public static void main(String[] args) {
        CarToyota carToyota = new CarToyota();
        CarBMW carBMW = new CarBMW();

        carToyota.run();
        carBMW.run();
    }
}

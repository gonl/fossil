package com.qa.automation.fossil.question.question4.Objects;

public class Car implements Runnable {
    public int wheels;
    public int doors;
    public int seats;
    public int maxSpeed;

    public Car() {
        this.wheels = 4;
        this.doors = 4;
        this.seats = 5;
    }

    private void run2() {
        synchronized (CarToyota.class) {
            synchronized (CarBMW.class) {
                System.out.println(this.maxSpeed);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            run2();
        }
    }
}

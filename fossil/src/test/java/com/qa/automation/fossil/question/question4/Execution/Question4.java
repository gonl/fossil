package com.qa.automation.fossil.question.question4.Execution;

import com.qa.automation.fossil.question.question4.Objects.CarBMW;
import com.qa.automation.fossil.question.question4.Objects.CarToyota;

public class Question4 {

    public static void main(String[] args) {
        Thread threadToyota = new Thread(new CarToyota());
        Thread threadBMW = new Thread(new CarBMW());

        threadToyota.start();
        threadBMW.start();

    }
}

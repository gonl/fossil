package com.qa.automation.fossil.question.question2.Execution;

import com.qa.automation.fossil.question.question2.Objects.CarBMW;
import com.qa.automation.fossil.question.question2.Objects.CarToyota;

public class Question2 {

    public static void main(String[] args) throws InterruptedException {
        Thread threadToyota = new Thread(new CarToyota());
        Thread threadBMW = new Thread(new CarBMW());

        threadToyota.start();
        threadBMW.start();
    }
}

package com.qa.automation.fossil.question.question3.Execution;

import com.qa.automation.fossil.question.question3.Objects.CarBMW;
import com.qa.automation.fossil.question.question3.Objects.CarToyota;

public class Question3 {

    public static void main(String[] args) throws InterruptedException {
        Thread threadToyota = new Thread(new CarToyota());
        Thread threadBMW = new Thread(new CarBMW());



        threadToyota.start();
        threadToyota.join();
        threadBMW.start();
    }
}
